const http = require('http');
const fs = require('fs');
const mysql = require('mysql');
const io = require('socket.io');
const regs = require('./libs/regs')

//数据库 连接池
let db = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root', 
  database: 'websock'
});

// ----------------------- 1. 创建http服务器 --------------------
let httpServer = http.createServer((req, res) => {
  fs.readFile(`www${req.url}`, (err, data) => {
    if (err) {
      res.writeHeader(404);
      res.write('not found');
    } else {
      res.write(data);
    }

    res.end();
  });
});
httpServer.listen(8888);

// -----------------------  2. 开启WebSocket服务器 ----------------------

let userGroup = [];  // 每一个 登录用户，保存区

let wsServer = io.listen(httpServer);
wsServer.on('connection', sock => {

  userGroup.push(sock); // 用户登录一个，存取一个

  let cur_username = '';  // 自定义用户 初始状态
  let cur_userID = 0;

  // ------------------------ 注册·接口 --------------------------------
  sock.on('reg', (user, pass) => {
    //1.校验数据
    if (!regs.username.test(user)) {
      sock.emit('reg_ret', 1, '用户名不符合规范');
    } else if (!regs.password.test(pass)) {
      sock.emit('reg_ret', 1, '密码不符合规范');
    } else {
      //2.用户名是否存在
      db.query(`SELECT ID FROM user_infos WHERE username='${user}'`, (err, data) => {
        if (err) {
          sock.emit('reg_ret', 1, '数据库有错误');
        } else if (data.length > 0) {
          sock.emit('reg_ret', 1, '用户名已存在');
        } else {
          //3.插入
          db.query(`INSERT INTO user_infos (username, password, status) VALUES('${user}','${pass}', 0)`, err => {
            if (err) {
              sock.emit('reg_ret', 1, '数据库有错误');
            } else {
              sock.emit('reg_ret', 0, '注册成功');
            }
          });
        }
      });
    }


  });

  // -------------------------------- 登陆·接口 --------------------------------
  sock.on('login', (user, pass) => {
    //1.校验数据
    if (!regs.username.test(user)) {
      sock.emit('login_ret', 1, '用户名不符合规范');
    } else if (!regs.password.test(user)) {
      sock.emit('login_ret', 1, '密码不符合规范');
    } else {
      //2.用户信息
      db.query(`SELECT ID,password FROM user_infos WHERE username='${user}'`, (err, data) => {
        if (err) {
          sock.emit('login_ret', 1, '数据库有错');
        } else if (data.length == 0) {
          sock.emit('login_ret', 1, '此用户不存在');
        } else if (data[0].password != pass) {
          sock.emit('login_ret', 1, '用户名或密码有误');
        } else {
          //3.改在线状态
          db.query(`UPDATE user_infos SET status=1 WHERE ID=${data[0].ID}`, err => {
            if (err) {
              sock.emit('login_ret', 1, '数据库有错');
            } else {
              sock.emit('login_ret', 0, '登陆成功');
              cur_username = user;
              cur_userID = data[0].ID;
            }
          });
        }
      });
    }


  });

  // - ------------------------- 发言 ---------------------
  sock.on('msg', txt => {
    if (!txt) {
      sock.emit('msg_ret', 1, '消息文本不能为空');
    } else if (cur_username == '') {
      sock.emit('msg_ret', 1, '您还未登录，不能发言')
    } else {
      //广播给所有人
      userGroup.forEach(item => {
        if (item == sock) return;

        item.emit('msg', cur_username, txt);
      });

      sock.emit('msg_ret', 0, '发送成功');
    }
  });

  // ----------------- 关闭浏览器 离线 ------------------
  sock.on('disconnect', function () {
    db.query(`UPDATE user_infos SET status=0 WHERE ID=${cur_userID}`, err => {
      if (err) {
        console.log('数据库有错', err);
      }

      cur_username = '';
      cur_userID = 0;

      userGroup = userGroup.filter(item => item != sock);
    });
  });

  // ------------------ 按钮 离线 ------------------------
  sock.on('offline', (user, pass) => {
    db.query(`select * from user_infos where username='${user}'`, (err, data) => {
      if (err) {
        sock.emit('offline_ret', 1, '操作失败')
      } else if (data[0].status == 1) {
        db.query(`update user_infos set status=0 where id='${data[0].id}'`, (err, data) => {
          console.log(data)
          if (err) {
            sock.emit('offline_ret', 1, '下线操作失败')
          } else {
            sock.emit('offline_ret', 0, '下线成功')
          }
        })
      } else if (data[0].status == 0) {
        sock.emit('offline_ret', 1, '该用户还未登录')
      }
    })
  })

  // ------------------- 注销账户 -------------------------
  sock.on('delete', (user, pass) => {
    // 获取当前的 登录用户信息
    db.query(`select * from user_infos where id='${cur_userID}'`, (err, data) => {
      if (err) {
        sock.emit('del_ret', 1, '获取登录用户信息失败')
      } else if (data.length > 0) {
        db.query(`delete from user_infos where id='${cur_userID}'`, err => {
          if (err) {
            sock.emit('del_ret', 1, '数据库err')
          } else {
            sock.emit('del_ret', 0, '注销成功')
            cur_username = ''
          }
        })
      } else { 
        sock.emit('del_ret', 1, '该用户未登录或不存在')
      }
    })
  })


});
