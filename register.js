// 引入 必要 模块
const http = require('http')
const fs = require('fs')
const url = require('url')
const mysql = require('mysql')
const regs = require('./libs/regs')

// 数据库连接池
let db = mysql.createPool({
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: 'root',
  database: 'websock'
})

// 建立服务器连接
let httpServer = http.createServer((req, res) => {

  // 获取地址信息
  let { pathname, query } = url.parse(req.url, true)
  // console.log(url.parse(req.url, true))

  // 注册接口
  if (pathname == '/reg') {
    let { user, pass } = query
    // console.log(query)
    // 判断数据库信息
    if (!regs.username.test(user)) {
      res.write(JSON.stringify({ code: 1, msg: '用户名不符合规范' }));
      res.end();
    } else if (!regs.password.test(pass)) {
      res.write(JSON.stringify({ code: 1, msg: '密码不符合规范' }));
      res.end();
    } else {
      db.query(`SELECT * FROM user_infos WHERE username='${user}'`, (err, data) => {
        console.log(data)
        if (err) {
          res.write(JSON.stringify({ code: 1, msg: '数据库获取信息失败' }))
          res.end()
        } else if (data.length > 0) {
          res.write(JSON.stringify({ code: 1, msg: '用户名已存在，请重新输入' }))
          res.end()
        } else {
          db.query(`INSERT INTO user_infos (username,password,status) VALUES('${user}','${pass}','0')`, err => {
            if (err) {
              res.write(JSON.stringify({ code: 1, msg: '数据库添加信息出现异常' }))
              res.end()
            } else {
              res.write(JSON.stringify({ code: 0, msg: '注册成功' }))
              res.end()
            }
          })
        }
      })
    }
    // 登录接口
  } else if (pathname == '/login') {
    let { user, pass } = query
    db.query(`SELECT * FROM user_infos WHERE username='${user}'`, (err, data) => {
      console.log(data)
      if (err) {
        res.write(JSON.stringify({ code: 1, msg: '数据库查询信息失败' }));
        res.end();
      } else if (data.length == 0) {
        res.write(JSON.stringify({ code: 1, msg: '此用户不存在' }));
        res.end();
      } else if (data[0].password != pass) {
        res.write(JSON.stringify({ code: 1, msg: '登陆失败，密码或用户名错误' }));
        res.end();
      } else { 
        // 设置登录状态
        db.query(`UPDATE user_infos SET status=1 WHERE ID='${data[0].id}'`, err => {
          if (err) {
            res.write(JSON.stringify({ code: 1, msg: '数据库设置状态失败' }))
            res.end()
          } else {
            res.write(JSON.stringify({ code: 0, msg: '登陆成功' }))
            res.end()
          }
        })
      }
    })
  } else {
    fs.readFile(`www${pathname}`, (err, data) => {
      if (err) {
        res.write('文件报错')
        res.writeHeader(404)
      } else {
        res.write(data)
      }
      res.end()
    })
  }
})

httpServer.listen(3000)