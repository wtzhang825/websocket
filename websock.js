// 引入 所需 模块
const http = require('http')
const mysql = require('mysql')
const fs = require('fs')
const io = require('socket.io')
const regs = require('./libs/regs')

// 数据库 连接池
let db = mysql.createPool({
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: 'root',
  database: 'websock'
})

// 创建 http 服务器
let httpServer = http.createServer((req, res) => {
  fs.readFile(`www${req.url}`, (err, data) => {
    if (err) {
      res.write('Not found file')
      res.writeHeader(404)
    } else {
      res.write(data)
    }
    res.end()
  })
}).listen(3002)

// 创建 websocket 服务器
let wsServer = io.listen(httpServer)
wsServer.on('connection', sock => {
  // 保存当前上线用户信息
  let current_user = ''
  let current_userId = 0
  // ---------------------- 注册接口 -----------------------
  sock.on('reg', (user, pass) => {
    // 校验数据
    if (!regs.username.test(user)) {
      sock.emit('reg_ret', 1, '用户名不符合规范')
    } else if (!regs.password.test(pass)) {
      sock.emit('reg_ret', 1, '密码不符合规范')
    } else {
      // 用户名是否存在
      db.query(`select * from user_infos where username='${user}'`, (err, data) => {
        console.log(data[0])
        if (err) {
          sock.emit('reg_ret', 1, '数据库获取信息出错')
        } else if (data.length > 0) {
          sock.emit('reg_ret', 1, '该用户名已经存在，请重新输入')
        } else {
          // 添加注册信息
          db.query(`insert into user_infos (username,password,status) values('${user}','${pass}','0')`, (err, data) => {
            if (err) {
              sock.emit('reg_ret', 1, '数据库添加信息失败')
            } else {
              sock.emit('reg_ret', 0, '注册成功')
            }
          })
        }
      })
    }
  })
  // ------------------------------- 登录接口 ------------------------
  sock.on('login', (user, pass) => {
    // 结合数据库 验证用户信息
    db.query(`select * from user_infos where username='${user}'`, (err, data) => {
      if (err) {
        sock.emit('login_ret', 1, '数据库验证信息失败')
      } else if (data.length == 0) {
        sock.emit('login_ret', 1, '用户名不存在或密码出现错误')
      } else {
        // 设置 登录状态 在线
        db.query(`update user_infos set status=1 where ID='${data[0].id}'`, err => {
          // console.log(data)
          if (err) {
            sock.emit('login_ret', 1, '设置状态失败')
          } else {
            sock.emit('login_ret', 0, '登录成功')
            // 保存状态 信息
            current_user = user
            current_userId = data[0].id
          }
        })
      }
    })
  })
  // 关闭浏览器 离线
  sock.on('disconnect', function () {
    db.query(`update user_infos set status=0 where ID=${current_userId}`, err => {
      if (err) {
        console.log('操作失败')
      } else {
        console.log('已下线成功')
        current_user = ''
        current_userId = 0
      }
    })
  })
  // 点击按钮 离线
  sock.on('offline', (user, pass) => {
    // 查询 数据库 该用户登录 状态
    db.query(`select * from user_infos where username='${user}'`, (err, data) => {
      console.log(data.status)
      if (err) {
        sock.emit('offline_ret', 1, '操作失败')
      } else if (data[0].status == 1) {
        db.query(`update user_infos set status=0 where ID='${data[0].id}'`, (err, data) => {
          console.log(data)
          if (err) {
            sock.emit('offline_ret', 1, '下线操作失败')
          } else {
            sock.emit('offline_ret', 0, '下线成功')
          }
        })
      } else if (data[0].status == 0) {
        sock.emit('offline_ret', 1, '该用户还未登录')
      } 
    })
  })
})