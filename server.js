const http = require('http')
const io = require('socket.io')

// 搭建 http 服务器环境
const httpServer = http.createServer()
httpServer.listen(3000)

// 搭建 websock 服务
const wsServer = io.listen(httpServer)
wsServer.on('connection', sock => { 
  // sock.emit
  sock.on('a', (...args) => { 
    console.log(...args)
  })
}) 