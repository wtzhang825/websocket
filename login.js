// 引入必要 模块
const http = require('http')
const mysql = require('mysql')
const fs = require('fs')
const url = require('url')
const regs = require('./libs/regs')


// 数据库 连接池
let db = mysql.createPool({
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: 'root',
  database: 'websock'
})

// 创建 服务器
let httpServer = http.createServer((req, res) => {
  // res.write('success')
  // res.end()

  // 动态获取 路径信息
  let { pathname, query } = url.parse(req.url, true)
  // console.log(url.parse(req.url, true))

  // 判断路径接口
  // --------------------------- 注册接口 --------------------------------
  if (pathname == '/reg') {
    // 获取注册信息
    let { user, pass } = query
    // console.log(user)
    // 用户注册信息 格式规范 校验
    if (!regs.username.test(user)) {
      res.write({ code: 1, msg: '用户名格式不准确，请以4位以上的字母数字组成' })
      res.end()
    } else if (!regs.password.test(pass)) {
      res.write({ code: 1, msg: '密码格式不准确，请以6位以上的数字组成' })
      res.end()
    } else {
      // 获取数据库信息，判断用户是否存在
      db.query(`SELECT * FROM user_infos WHERE username='${user}'`, (err, data) => {
        // console.log(data)
        if (err) {
          res.write(JSON.stringify({ code: 1, msg: '数据库查询数据出现异常' }))
          res.end()
        } else if (data.length > 0) {
          res.write(JSON.stringify({ code: 1, msg: '用户名已经存在，请重新输入' }))
          res.end()
        } else {
          // 插入 注册信息
          db.query(`INSERT INTO user_infos (username,password,status) VALUES('${user}','${pass}','0')`, err => {
            if (err) {
              res.write(JSON.stringify({ code: 1, msg: '数据库插入信息失败' }))
              res.end()
            } else {
              res.write(JSON.stringify({ code: 0, msg: '注册成功' }))
              res.end()
            }
          })
        }
      })
    }
    // -------------------------- 登录接口 ---------------------------------
  } else if (pathname == '/login') {
    let { user, pass } = query
    // 获取用户信息
    db.query(`SELECT * FROM user_infos WHERE username='${user}'`, (err, data) => {
      if (err) {
        res.write(JSON.stringify({ code: 1, msg: '获取用户信息失败' }))
        res.end()
      } else if (data.length == 0) {
        res.write(JSON.stringify({ code: 1, msg: '用户名或密码错误' }))
        res.end()
      } else {
        // 设置 登录状态
        db.query(`UPDATE user_infos SET status=1 WHERE ID='${data[0].id}'`, (err, data) => {
          if (err) {
            res.write(JSON.stringify({ code: 1, msg: '登录状态设置失败' }))
            res.end()
          } else {
            res.write(JSON.stringify({ code: 0, msg: '登陆成功' }))
            res.end()
          }
        })
      }
    })
  } else {
    fs.readFile(`www${pathname}`, (err, data) => {
      if (err) {
        res.write('文件飞走了，erro~~')
        res.writeHead(404)
      } else {
        res.write(data)
      }
      res.end()
    })
  }

})

httpServer.listen(3001)